<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página de Inicio - Plataforma Deportiva</title>
    <link rel="stylesheet" href="PaginaInicio.css">
</head>
<body>
    <header>
        <div class="scrolling-text">
            <p>"Sumérgete en el apasionante mundo del deporte, disfruta de los mejores eventos en vivo y en directo, todo completamente gratis!"</p>
        </div>
    </header>
    <div class="overlay">
        <div class="content-box">
            <h1 class="tracking-in-expand">Plataforma Deportiva</h1>
            <p class="tracking-in-expand-fwd">¡Disfruta de tus deportes favoritos gratis!</p>
           
            <main>
                <div class="buttons">
                    <a href="InicioSesion.php" class="login-btn">Iniciar sesión</a>
                    <a href="Registro.php" class="register-btn">Registrarse</a>
                    <a href="Soporte.php" class="support-btn">Soporte</a> <!-- Nuevo botón de Soporte -->
                </div>
            </main>
        </div>
    </div>
</body>
</html>
