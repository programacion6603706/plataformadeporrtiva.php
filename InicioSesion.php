<?php session_start(); ?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iniciar Sesión - Plataforma Deportiva</title>
    <link rel="stylesheet" href="InicioSesion.css">
</head>

<body>
    <div class="overlay">
        <div class="content-box">
            <header>
                <h1>Iniciar Sesión</h1>
            </header>
            <form class="login-form" action="VerificarLogin.php" method="POST">
                <div class="input-group">
                    <label for="email">Correo electrónico:</label>
                    <input type="email" id="email" name="email" required>
                </div>
                <div class="input-group">
                    <label for="password">Contraseña:</label>
                    <input type="password" id="password" name="password" required>
                </div>

                <button type="submit" class="login-btn">Iniciar Sesión</button>
            </form>
            <div class="register-link">
                ¿No tienes cuenta? <a href="Registro.php">Regístrate</a>
            </div>
        </div>
    </div>
</body>

</html>