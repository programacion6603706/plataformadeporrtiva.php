<?php
session_start();

// Verificar si el usuario está autenticado
if (isset($_SESSION['usuario'])) {
    $usuario = $_SESSION['usuario'];
} else {
    // Si el usuario no está autenticado, redirigirlo a la página de inicio de sesión
    header("Location: InicioSesion.php");
    exit;
}
// Incluir el archivo que contiene la función baseconexion
require_once 'FuncionSql.php';
$conn = baseconexion();

// Obtener el ID del usuario de la sesión
$id_usuario = $_SESSION['usuario']['id'];

// Consulta SQL para obtener los datos del usuario
$sql = "SELECT * FROM usuarios WHERE id = $id_usuario";

// Ejecutar la consulta
$result = $conn->query($sql);

// Verificar si se obtuvieron resultados
if ($result->num_rows > 0) {
    // Obtener los datos del usuario
    $usuario = $result->fetch_assoc();
} else {
    // Si no se encuentran datos del usuario, mostrar un mensaje de error o redirigir a una página de error
    echo "Error: No se encontraron datos del usuario.";
    exit;
}

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Datos de Cuenta</title>
    <link rel="stylesheet" href="ConfiguracionUser.css">
</head>

<body>
    <div class="container">
        <h2>Datos de Cuenta</h2>

        <p><strong>Nombre de Usuario:</strong> <?php echo isset($usuario['nickname']) ? $usuario['nickname'] : ''; ?>
        </p>
        <p><strong>Email:</strong> <?php echo isset($usuario['email']) ? $usuario['email'] : ''; ?></p>
        <p><strong>Rol:</strong> <?php echo isset($usuario['rol']) ? $usuario['rol'] : ''; ?></p>


        <h3>¿Quieres cambiar la contraseña?</h3>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <label for="contrasena">Nueva Contraseña:</label>
            <input type="password" id="contrasena" name="contrasena" required><br><br>

            <input type="submit" value="Cambiar Contraseña">
        </form>

        <h3>¿Quieres cambiar el nombre de usuario?</h3>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <label for="nuevo_nombre">Nuevo Nombre de Usuario:</label>
            <input type="text" id="nuevo_nombre" name="nuevo_nombre" required><br><br>

            <input type="submit" value="Cambiar Nombre">
        </form>
        <?php
        // Verificar si se reciben datos del formulario para cambiar la contraseña
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["contrasena"])) {
    // Verificar que la contraseña no esté vacía
    if (!empty($_POST["contrasena"])) {
        // Verificar si la contraseña contiene solo letras y números
        if (preg_match('/^[a-zA-Z0-9]+$/', $_POST["contrasena"])) {
            // Obtener el usuario de la sesión
            $usuario = $_SESSION['usuario'];

            // Obtener la nueva contraseña ingresada por el usuario
            $nueva_contrasena = $_POST["contrasena"];

            // Hashear la nueva contraseña
            $hashed_password = password_hash($nueva_contrasena, PASSWORD_DEFAULT);

            // Actualizar la contraseña del usuario en la base de datos
            $sql = "UPDATE usuarios SET hashed_password = '$hashed_password' WHERE id = {$usuario['id']}";

            if ($conn->query($sql) === TRUE) {
                echo "<p class='success'>Contraseña actualizada correctamente.</p>";
            } else {
                echo "<p class='error'>Error al actualizar la contraseña: " . $conn->error . "</p>";
            }
        } else {
            // La contraseña contiene caracteres no permitidos, mostrar un mensaje de error
            echo "<p class='error'>La contraseña solo puede contener letras y números.</p>";
        }
    } else {
        // La contraseña está vacía, mostrar un mensaje de error
        echo "<p class='error'>Por favor, ingrese una nueva contraseña para realizar cambios.</p>";
    }
}


// Verificar si se reciben datos del formulario para cambiar el nombre de usuario
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["nuevo_nombre"])) {
    // Verificar que el nuevo nombre de usuario no esté vacío
    if (!empty($_POST["nuevo_nombre"])) {
        // Obtener el usuario de la sesión
        $usuario = $_SESSION['usuario'];

        // Obtener el nuevo nombre de usuario ingresado por el usuario y eliminar espacios en blanco adicionales
        $nuevo_nombre = trim($_POST["nuevo_nombre"]);

        // Verificar si el nuevo nombre de usuario contiene solo letras y números
        if (preg_match('/^[a-zA-Z0-9]+$/', $nuevo_nombre)) {
            // Actualizar el nombre de usuario del usuario en la base de datos
            $sql = "UPDATE usuarios SET nickname = '$nuevo_nombre' WHERE id = {$usuario['id']}";

            if ($conn->query($sql) === TRUE) {
                echo "<p class='success'>Nombre de usuario actualizado correctamente.</p>";
            } else {
                echo "<p class='error'>Error al actualizar el nombre de usuario: " . $conn->error . "</p>";
            }
        } else {
            // El nuevo nombre de usuario contiene caracteres no permitidos, mostrar un mensaje de error
            echo "<p class='error'>El nombre de usuario solo puede contener letras y números.</p>";
        }
    } else {
        // El nuevo nombre de usuario está vacío, mostrar un mensaje de error
        echo "<p class='error'>Por favor, ingrese un nuevo nombre de usuario para realizar cambios.</p>";
    }
}

?>
    </div>
</body>

</html>