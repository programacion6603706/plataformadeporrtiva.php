<?php
session_start();

// Verificar si el usuario está autenticado
if (!isset($_SESSION['usuario'])) {
    // Si el usuario no está autenticado, redirigirlo a la página de inicio de sesión
    header("Location: PaginaInicio.php");
    exit;
}

require_once 'FuncionSql.php';
$conn = baseconexion();

// Verificar si se ha establecido una sesión de usuario
if (isset($_SESSION['usuario'])) {
    // Obtener el usuario de la sesión
    $usuario = $_SESSION['usuario'];

} else {
    // Si no hay sesión de usuario, redirigir a la página de inicio de sesión
    header("Location: PaginaInicio.php");
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página de Acceso</title>
    <link rel="stylesheet" href="Baloncesto.css">
</head>

<body>
    <div class="container">
        <h1 class="title">Página de Acceso</h1>
        <h2 class="slide-title">Baloncesto</h2>
        <div>
            <h1>Proximos partidos</h1>
        </div>
        <div class="matches">
            <a class="match" href="PartidosNBA/Madrid-Baskonia.php">
                <div class="match">
                    <img src="./Imagenes/Madrid_Bashonia.jfif" alt="Partido 1">
                    <h4>Real Madrid vs Bashonia</h4>
                </div>
            </a>
            <a class="match" href="PartidosNBA/Barca-Olympiacos.php">
                <div class="match">
                    <img src="./Imagenes/Barça_Olympiacos.jfif" alt="Partido 2">
                    <h4>Barça vs Olympiacos </h4>
                </div>
            </a>

            <a class="match" href="PartidosNBA/Madrid-Monbus.php">
                <div class="match">
                    <img src="./Imagenes/Madrid_Bonbus.jfif" alt="Partido 3">
                    <h4>Madrid vs Monbus</h4>
                </div>
            </a>
            <a class="match" href="PartidosNBA/Lakers-Denver.php">
                <div class="match">
                    <img src="./Imagenes/Lakers_Denver.jfif" alt="Partido 4">
                    <h4>Lakers vs Denver</h4>
                </div>
            </a>
            <a class="match" href="PartidosNBA/Dallas-Clipper.php">
                <div class="match">
                    <img src="./Imagenes/Dallas_Clipper.jfif" alt="Partido 5">
                    <h4>Dallas vs Clipper</h4>
                </div>
            </a>

        </div>

        <div>
            <h1>Mejores Partidos</h1>
        </div>
        <div></div>
        <div class="matches">

            <a class="match" href="PartidosNBA/Bulls-Atlanta.php">
                <div class="replay">
                    <img src="./Imagenes/Chicago Bulls - Atlanta Hawks.jfif" alt="Repetición 1">
                    <h4>Chicago Bulls vs Atlanta Hawks</h4>
                </div>
            </a>
            <a class="match" href="PartidosNBA/Pelicans-Kings.php">
                <div class="replay">
                    <img src="./Imagenes/New Orleans Pelicans - Sacramento Kings.jfif" alt="Repetición 2">
                    <h4> New Orleans Pelicans vs Sacramento Kings</h4>
                </div>
            </a>
            <a class="match" href="PartidosNBA/76ers-Knicks.php">
                <div class="replay">
                    <img src="./Imagenes/Philadelphia 76ers - New York Knicks.jfif" alt="Repetición 3">
                    <h4>Philadelphia 76ers vs New York Knicks</h4>
                </div>
            </a>
            <a class="match" href="PartidosNBA/Bucks-Indiana.php">
                <div class="replay">
                    <img src="./Imagenes/Milwaukee Bucks- Indiana Pacers.jfif" alt="Repetición 4">
                    <h4>Milwaukee Bucks vs Indiana Pacers</h4>
                </div>
            </a>
            <a class="match" href="PartidosNBA/Cavaliers-Orlando.php">
                <div class="replay">
                    <img src="./Imagenes/Cleveland Cavaliers - Orlando Magic.jfif" alt="Repetición 5">
                    <h4>Cleveland Cavaliers vs Orlando Magic</h4>
                </div>
            </a>

        </div>
    </div>
    </div>
</body>

</html>