<?php
session_start();

// Cerrar la sesión
session_unset();
session_destroy();

// Redireccionar a PaginaInicio.php después de cerrar sesión
header("Location: PaginaInicio.php");
exit;
?>
