<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrarse - Plataforma Deportiva</title>
    <link rel="stylesheet" href="Registro.css">
</head>

<body>
    <div class="overlay">
        <div class="content-box">
            <header>
                <h1>Registrarse</h1>
            </header>
            <form class="register-form" action="VerificarRegistro.php" method="POST">
                <div class="input-group">
                    <div class="name-inputs">
                        <div class="input-group">
                            <label for="firstname">Nombre:</label>
                            <input class="inputNombre" type="text" id="firstname" name="firstname" required>
                        </div>
                        <div class="input-group">
                            <label for="lastname">Apellido:</label>
                            <input class="inputNombre" type="text" id="lastname" name="lastname" required>
                        </div>
                    </div>
                </div>
                <div class="input-group">
                    <label for="email">Correo electrónico:</label>
                    <input type="email" id="email" name="email" required>
                </div>
                <div class="input-group">
                    <label for="nickname">Nombre de usuario:</label>
                    <input type="text" id="nickname" name="nickname" required>
                </div>
                <div class="input-group">
                    <div class="password-inputs">
                        <div class="input-group">
                            <label for="password">Contraseña:</label>
                            <input type="password" id="password" name="password" required>
                        </div>
                        <div class="input-group">
                            <label for="password2">Repetir contraseña:</label>
                            <input type="password" id="password2" name="password2" required>
                        </div>
                    </div>
                </div>
                <button type="submit" class="register-btn">Registrarse</button>
            </form>
            <div class="login-link">
                ¿Ya tienes una cuenta? <a href="InicioSesion.php">Inicia sesión</a>
            </div>
        </div>
    </div>
    <?php
    if (isset($_SESSION['error'])) {
        echo "<p class='error'>{$_SESSION['error']}</p>";
        unset($_SESSION['error']);
    }
    ?>
</body>

</html>
