<?php
session_start();

// Verificar si el usuario está autenticado
if (!isset($_SESSION['usuario'])) {
    // Si el usuario no está autenticado, redirigirlo a la página de inicio de sesión
    header("Location: PaginaInicio.php");
    exit;
}

require_once 'FuncionSql.php';
$conn = baseconexion();

// Verificar si se ha establecido una sesión de usuario
if (isset($_SESSION['usuario'])) {
    // Obtener el usuario de la sesión
    $usuario = $_SESSION['usuario'];


} else {
    // Si no hay sesión de usuario, redirigir a la página de inicio de sesión
    header("Location: PaginaInicio.php");
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página de Acceso</title>
    <link rel="stylesheet" href="UFC.css">
</head>

<body>
    <div class="container">
        <h1 class="title">Página de Acceso</h1>
        <h2 class="slide-title">UFC</h2>
        <div>
            <h1>Proximas Peleas</h1>
        </div>
        <div class="matches">
            <a class="match" href="PartidosUFC/Mikel-Wattana.php">
                <div class="match">
                    <img src="./Imagenes/MIKEL FERNÁNDEZ VS. WATTANA.webp" alt="Partido 1">
                    <h4>MIKEL FERNÁNDEZ VS. WATTANA</h4>
                </div>
            </a>
            <a class="match" href="PartidosUFC/Oktagon56.php">
                <div class="match">
                    <img src="./Imagenes/OKTAGON 56.webp" alt="Partido 2">
                    <h4>OKTAGON 56</h4>
                </div>

            </a>
            <a class="match" href="PartidosUFC/PFL3.php">

                <div class="match">
                    <img src="./Imagenes/PFL 3.webp" alt="Partido 3">
                    <h4>PFL 3</h4>
                </div>
            </a>
            <a class="match" href="PartidosUFC/Oktagon57.php">
                <div class="match">
                    <img src="./Imagenes/OKTAGON 57.webp" alt="Partido 4">
                    <h4>OKTAGON 57c</h4>
                </div>
            </a>
            <a class="match" href="PartidosUFC/Nicolau-Perez.php">

                <div class="match">
                    <img src="./Imagenes/UFC FIGHT NIGHT  NICOLAU VS. PÉREZ.webp" alt="Partido 5">
                    <h4>UFC FIGHT NIGHT | NICOLAU VS. PÉREZ</h4>
                </div>
            </a>

        </div>

        <div>
            <h1>Mejores Peleas</h1>
        </div>
        <div></div>
        <div class="matches">
            <a class="match" href="PartidosUFC/Toupuria-Volkanovsky.php">
                <div class="replay">

                    <img src="./Imagenes/ILIA TOPURIA.webp" alt="Repetición 1">
                    <h4> ILIA TOPURIA vs VOLKANOVSKI</h4>
                </div>
            </a>
            <a class="match" href="PartidosUFC/BraveCf-81.php">
                <div class="replay">
                <img src="./Imagenes/BRAVE CF 81 I LJUBLJANA.webp" alt="Repetición 2">
                <h4>BRAVE CF 81 I LJUBLJANA</h4>
            </div>
            </a>
            <a class="match" href="PartidosUFC/PFL3-pesaje.php">
                <div class="replay">
                <img src="./Imagenes/PFL 3  PESAJE.webp" alt="Repetición 3">
                <h4>PFL 3 | PESAJE</h4>
            </div>
            </a>
            <a class="match" href="PartidosUFC/Ryan-Victor.php">
               <div class="replay">
                <img src="./Imagenes/JOE RYAN VS. VICTOR HUGO.webp" alt="Repetición 4">
                <h4>JOE RYAN VS VICTOR HUGO</h4>
            </div> 
            </a>
            <a class="match" href="PartidosUFC/PFL10-PlayOffFinals.php">
                <div class="replay">
                <img src="./Imagenes/PFL 10 PLAYOFF FINALS.webp" alt="Repetición 5">
                <h4>PFL 10: PLAYOFF FINALS</h4>
            </div>
            </a>
            
        </div>
    </div>
    </div>
</body>

</html>