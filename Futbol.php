<?php
session_start();

// Verificar si el usuario está autenticado
if (!isset($_SESSION['usuario'])) {
    // Si el usuario no está autenticado, redirigirlo a la página de inicio de sesión
    header("Location: PaginaInicio.php");
    exit;
}

// Incluir el archivo que contiene la función baseconexion
require_once 'FuncionSql.php';
$conn = baseconexion();

// Verificar si se ha establecido una sesión de usuario
if (isset($_SESSION['usuario'])) {
    // Obtener el usuario de la sesión
    $usuario = $_SESSION['usuario'];

} else {
    // Si no hay sesión de usuario, redirigir a la página de inicio de sesión
    header("Location: PaginaInicio.php");
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página de Acceso</title>
    <link rel="stylesheet" href="Futbol.css">
</head>

<body>
    <div class="container">
        <h1 class="title">Página de Acceso</h1>
        <h2 class="slide-title">Fútbol</h2>
        <div>
            <h1>Próximos partidos</h1>
        </div>
        <div class="matches">

            <a class="match" href="PartidosFutbol/Madrid-Real.php">
                <div class="match">
                    <img src="./Imagenes/Realsociedad_Madrid.jfif" alt="Partido 1">
                    <h4>Real Madrid vs Real Sociedad</h4>
                </div>
            </a>
            <a class="match" href="PartidosFutbol/Atleti-Bilbao.php">
                <div class="match">
                    <img src="./Imagenes/Atleti_Athletic.jfif" alt="Partido 2">
                    <h4>Atlético de Madrid vs Athletic Club</h4>
                </div>
            </a>
            <a class="match" href="PartidosFutbol/Juventus-Milan.php">
                <div class="match">
                    <img src="./Imagenes/Juve_Milan.jfif" alt="Partido 3">
                    <h4>Juventus vs Milan</h4>
                </div>
            </a>
            <a class="match" href="PartidosFutbol/Bolonia-Udinese.php">
                <div class="match">
                    <img src="./Imagenes/Bolonia_Udinese.jfif" alt="Partido 4">
                    <h4>Bolonia vs Udinese</h4>
                </div>
            </a>
            <a class="match" href="PartidosFutbol/United-Burley.php">
                <div class="match">
                    <img src="./Imagenes/United-Burnley.jfif" alt="Partido 5">
                    <h4>Manchester United vs Burnley</h4>
                </div>
            </a>

        </div>

        <div>
            <h1>Mejores Partidos</h1>
        </div>
        <div class="matches">
            <a class="match" href="PartidosFutbol/City-Madrid.php">
                <div class="replay">
                    <img src="./Imagenes/City_Madrid.jfif" alt="Repetición 1">
                    <h4>Manchester City vs Real Madrid</h4>
                </div>
            </a>
            <a class="match" href="PartidosFutbol/Madrid-Barca.php">
                <div class="replay">
                    <img src="./Imagenes/Madrid_Barsa.jfif" alt="Repetición 2">
                    <h4>Real Madrid vs Barcelona</h4>
                </div>
            </a>
            <a class="match" href="PartidosFutbol/Dortmund-Atleti.php">
                <div class="replay">
                    <img src="./Imagenes/Atleti_dortmund.jfif" alt="Repetición 3">
                    <h4>Borussia Dortmund vs Atlético de Madrid</h4>
                </div>
            </a>
            <a class="match" href="PartidosFutbol/Rayo-Sevilla.php">
                <div class="replay">
                    <img src="./Imagenes/Rayo_Sevilla.jfif" alt="Repetición 4">
                    <h4>Rayo Vallecano vs Sevilla</h4>
                </div>
            </a>
            <a class="match" href="PartidosFutbol/Valencia-Betis.php">
                <div class="replay">
                    <img src="./Imagenes/Valencia_Betis.jfif" alt="Repetición 5">
                    <h4>Valencia vs Betis</h4>
                </div>
            </a>
        </div>
    </div>
</body>

</html>
