<?php
session_start();

// Verificar si el usuario está autenticado
if (!isset($_SESSION['usuario'])) {
    // Si el usuario no está autenticado, redirigirlo a la página de inicio de sesión
    header("Location: PaginaInicio.php");
    exit;
}

require_once 'FuncionSql.php';
$conn = baseconexion();

// Verificar si se ha establecido una sesión de usuario
if (isset($_SESSION['usuario'])) {
    // Obtener el usuario de la sesión
    $usuario = $_SESSION['usuario'];


} else {
    // Si no hay sesión de usuario, redirigir a la página de inicio de sesión
    header("Location: PaginaInicio.php");
    exit;
}

// Consultar todos los usuarios registrados
$sql = "SELECT id, nombre, email FROM usuarios";
$result = $conn->query($sql);

// Verificar si se recibió una solicitud de eliminación de usuario
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["eliminar_usuario"])) {
    // Obtener el ID del usuario a eliminar
    $id_usuario = $_POST["id_usuario"];

    // Eliminar el usuario de la base de datos
    $sql_delete = "DELETE FROM usuarios WHERE id = $id_usuario";

    if ($conn->query($sql_delete) === TRUE) {
        echo "<p class='success'>Usuario eliminado correctamente.</p>";
    } else {
        echo "<p class='error'>Error al eliminar usuario: " . $conn->error . "</p>";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Usuarios Registrados</title>
    <link rel="stylesheet" href="ConfiguracionAdmin.css">
</head>
<body>
    <div class="container">
        <h2>Usuarios Registrados</h2>
        <table>
            <tr>
                <th>Nombre de Usuario</th>
                <th>Email</th>
                <th>Acción</th>
            </tr>
            <a href="OpcionesAdmin.php">Salir</a>
            <?php
            // Mostrar la lista de usuarios
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td>" . $row["nombre"] . "</td>";
                    echo "<td>" . $row["email"] . "</td>";
                    echo "<td><form method='post'><input type='hidden' name='id_usuario' value='" . $row["id"] . "'><input type='submit' name='eliminar_usuario' value='Eliminar'></form></td>";
                    echo "</tr>";
                }
            } else {
                echo "<tr><td colspan='3'>No hay usuarios registrados.</td></tr>";
            }
            ?>
        </table>
    </div>
</body>
</html>

