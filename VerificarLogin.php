<?php
session_start();

// Incluir la definición de la clase Password
class Password {
    public static function hash($password) {
        return password_hash($password, PASSWORD_DEFAULT, ['cost' => 15]);
    }
    
    public static function verify($password, $hash) {
        return password_verify($password, $hash);
    }
}

// Incluir el archivo que contiene la función baseconexion
require_once 'FuncionSql.php';
$conn = baseconexion();

$email = $_POST['email'];
$raw_password = $_POST['password'];

// Eliminar espacios en blanco adicionales de la contraseña
$password = trim($raw_password);

// Verificar que la contraseña contenga solo letras y números
if (!preg_match('/^[a-zA-Z0-9]+$/', $password)) {
    $_SESSION['error'] = "La contraseña solo puede contener letras y números.";
    header("Location: InicioSesion.php");
    exit();
}

// Construir la consulta SQL
$sql = "SELECT id, hashed_password, rol FROM usuarios WHERE email='$email'";

// Ejecutar la consulta
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $hashed_password = $row['hashed_password'];
    
    if (Password::verify($password, $hashed_password)) {
        // Contraseña correcta
        $_SESSION['usuario'] = $row; // Almacena la información del usuario en la sesión
        if ($row['rol'] === 'admin') {
            // Redirigir a OpcionesAdmin.php si el usuario tiene rol de administrador
            header("Location: OpcionesAdmin.php");
            exit();
        } else {
            // Redirigir a Opciones.php si el usuario no es administrador
            header("Location: Opciones.php");
            exit();
        }
    } else {
        // Contraseña incorrecta, redirigir a InicioSesion.php con error
        $_SESSION['error'] = "Contraseña incorrecta.";
        header("Location: InicioSesion.php");
        exit();
    }
} else {
    // Usuario no encontrado, redirigir a Registro.php con error
    $_SESSION['error'] = "Usuario no encontrado.";
    header("Location: Registro.php");
    exit();
}

$conn->close();
?>
