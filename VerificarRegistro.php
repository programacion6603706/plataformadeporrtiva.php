<?php
session_start();

class Password {
    public static function hash($password) {
        return password_hash($password, PASSWORD_DEFAULT, ['cost' => 15]);
    }
    
    public static function verify($password, $hash) {
        return password_verify($password, $hash);
    }
}

// Incluir el archivo que contiene la función baseconexion
require_once 'FuncionSql.php';
$conn = baseconexion();

$nombre = $_POST['firstname'];
$apellidos = $_POST['lastname'];
$email = $_POST['email'];
$raw_password = $_POST['password'];
$nickname = $_POST['nickname']; 

// Validar y limpiar el nombre
$nombre = trim($_POST['firstname']);
if (!preg_match('/^[a-zA-Z]+$/', $nombre)) {
    $_SESSION['error'] = "El nombre solo puede contener letras.";
    header("Location: Registro.php");
    exit();
}

// Validar y limpiar los apellidos
$apellidos = trim($_POST['lastname']);
if (!preg_match('/^[a-zA-Z]+$/', $apellidos)) {
    $_SESSION['error'] = "Los apellidos solo pueden contener letras.";
    header("Location: Registro.php");
    exit();
}

// Validar y limpiar el nombre de usuario
$nickname = trim($_POST['nickname']);
if (!preg_match('/^[a-zA-Z0-9_]+$/', $nickname)) {
    $_SESSION['error'] = "El nombre de usuario solo puede contener letras, números y guiones bajos.";
    header("Location: Registro.php");
    exit();
}

// Eliminar espacios en blanco adicionales de la contraseña
$password = trim($raw_password);

// Verificar que la contraseña contenga solo letras y números
if (!preg_match('/^[a-zA-Z0-9]+$/', $password)) {
    $_SESSION['error'] = "La contraseña solo puede contener letras y números.";
    header("Location: Registro.php");
    exit();
}

// Hash de la contraseña
$hashed_password = Password::hash($password);

// Verificar si el email ya existe en la base de datos
$sql = "SELECT id FROM usuarios WHERE email='$email'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // Email ya registrado, redirigir al registro con un error
    $_SESSION['error'] = "El email ya está registrado.";
    header("Location: InicioSesion.php");
    exit();
} else {
    // Email no registrado, guardar el nuevo usuario en la base de datos
    $sql = "INSERT INTO usuarios (nombre, apellidos, email, hashed_password, nickname) 
            VALUES ('$nombre', '$apellidos', '$email', '$hashed_password', '$nickname')";
    
    if ($conn->query($sql) === TRUE) {
        // Usuario registrado con éxito, redirigir a mostrar.php
        $_SESSION['usuario'] = $conn->insert_id;
        header("Location: Opciones.php");
        exit();
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

$conn->close();
?>