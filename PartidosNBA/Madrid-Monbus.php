<?php
session_start();

// Verificar si el usuario está autenticado
if (!isset($_SESSION['usuario'])) {
    // Si el usuario no está autenticado, redirigirlo a la página de inicio de sesión
    header("Location: PaginaInicio.php");
    exit;
}
// Incluir el archivo que contiene la función baseconexion
require_once '../FuncionSql.php';
$conn = baseconexion();

// Obtener el ID del evento de la URL
$id_evento = 13;

// Consulta SQL para obtener los detalles del evento
$sql = "SELECT nombre, fecha_hora, lugar, descripcion FROM eventos WHERE id = $id_evento";

// Ejecutar la consulta
$result = $conn->query($sql);

if($result->num_rows > 0) {
    // Obtener los datos del evento
    $evento = $result->fetch_assoc();
    $nombre_evento = $evento['nombre'];
    $fecha = $evento['fecha_hora'];
    $lugar = $evento['lugar'];
    $descripcion = $evento['descripcion'];
} else {
    // Si no se encuentra el evento, redirigir a una página de error o mostrar un mensaje
    echo "El evento no se encontró.";
    exit;
}

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detalles del Evento</title>
    <link rel="stylesheet" href="PartidosBaloncesto.css">
</head>

<body>
    <div class="container">
        <div class="event-details">
            <div class="event-image">
                <img src="../Imagenes/balon-de-basquetbol_1020x1800_xtrafondos.com.jpg" alt="Evento" class="zoom">
            </div>
            <div class="event-info">
                <h1>Nombre del Evento: <?php echo $nombre_evento; ?></h1>
                <p>Fecha: <?php echo $fecha; ?></p>
                <p>Lugar: <?php echo $lugar; ?></p>
                <h3>Descripción:</h3>
                <p><?php echo $descripcion; ?></p>
            </div>
        </div>
    </div>
</body>

</html>