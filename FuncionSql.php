<?php
    
function baseconexion() {
    // Configuración de la base de datos
    $servername = "192.168.56.101:3306";
    $username = "admin";
    $password = "admin";
    $dbname = "DeportesApp";

    // Conexión a la base de datos
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Verificar conexión
    if ($conn->connect_error) {
        die("Error de conexión: " . $conn->connect_error);
    }

    return $conn; // Devuelve el objeto de conexión
}
?>
