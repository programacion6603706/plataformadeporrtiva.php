<?php
session_start();

// Verificar si el usuario está autenticado
if (!isset($_SESSION['usuario'])) {
    // Si el usuario no está autenticado, redirigirlo a la página de inicio de sesión
    header("Location: PaginaInicio.php");
    exit;
}

require_once 'FuncionSql.php';
$conn = baseconexion();

// Verificar si se ha establecido una sesión de usuario
if (isset($_SESSION['usuario'])) {
    // Obtener el usuario de la sesión
    $usuario = $_SESSION['usuario'];


} else {
    // Si no hay sesión de usuario, redirigir a la página de inicio de sesión
    header("Location: PaginaInicio.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tu Plataforma Deportiva</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="OpcionesAdmin.css">
</head>

<body>
    <div class="input">
        <form action="ConfiguracionUser.php" method="post">
            <button type="submit" class="value">
                <svg viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" data-name="Layer 2">
                    <path fill="#7D8590"
                        d="m1.5 13v1a.5.5 0 0 0 .3379.4731 18.9718 18.9718 0 0 0 6.1621 1.0269 18.9629 18.9629 0 0 0 6.1621-1.0269.5.5 0 0 0 .3379-.4731v-1a6.5083 6.5083 0 0 0 -4.461-6.1676 3.5 3.5 0 1 0 -4.078 0 6.5083 6.5083 0 0 0 -4.461 6.1676zm4-9a2.5 2.5 0 1 1 2.5 2.5 2.5026 2.5026 0 0 1 -2.5-2.5zm2.5 3.5a5.5066 5.5066 0 0 1 5.5 5.5v.6392a18.08 18.08 0 0 1 -11 0v-.6392a5.5066 5.5066 0 0 1 5.5-5.5z">
                    </path>
                </svg>
                Mi Cuenta
            </button>
        </form>
        <form action="ConfiguracionAdmin.php" method="post">
            <button type="submit" class="value">
                <svg id="Line" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
                    <path fill="#7D8590" id="XMLID_1646_"
                        d="m17.074 30h-2.148c-1.038 0-1.914-.811-1.994-1.846l-.125-1.635c-.687-.208-1.351-.484-1.985-.824l-1.246 1.067c-.788.677-1.98.631-2.715-.104l-1.52-1.52c-.734-.734-.78-1.927-.104-2.715l1.067-1.246c-.34-.635-.616-1.299-.824-1.985l-1.634-.125c-1.035-.079-1.846-.955-1.846-1.993v-2.148c0-1.038.811-1.914 1.846-1.994l1.635-.125c.208-.687.484-1.351.824-1.985l-1.068-1.247c-.676-.788-.631-1.98.104-2.715l1.52-1.52c.734-.734 1.927-.779 2.715-.104l1.246 1.067c.635-.34 1.299-.616 1.985-.824l.125-1.634c.08-1.034.956-1.845 1.994-1.845h2.148c1.038 0 1.914.811 1.994 1.846l.125 1.635c.687.208 1.351.484 1.985.824l1.246-1.067c.787-.676 1.98-.631 2.715.104l1.52 1.52c.734.734.78 1.927.104 2.715l-1.067 1.246c.34.635.616 1.299.824 1.985l1.634.125c1.035.079 1.846.955 1.846 1.993v2.148c0 1.038-.811 1.914-1.846 1.994l-1.635.125c-.208.687-.484 1.351-.824 1.985l1.067 1.246c.677.788.631 1.98-.104 2.715l-1.52 1.52c-.734.734-1.928.78-2.715.104l-1.246-1.067c-.635.34-1.299.616-1.985.824l-.125 1.634c-.079 1.035-.955 1.846-1.993 1.846zm-5.835-6.373c.848.53 1.768.912 2.734 1.135.426.099.739.462.772.898l.18 2.341 2.149-.001.18-2.34c.033-.437.347-.8.772-.898.967-.223 1.887-.604 2.734-1.135.371-.232.849-.197 1.181.089l1.784 1.529 1.52-1.52-1.529-1.784c-.285-.332-.321-.811-.089-1.181.53-.848.912-1.768 1.135-2.734.099-.426.462-.739.898-.772l2.341-.18h-.001v-2.148l-2.34-.18c-.437-.033-.8-.347-.898-.772-.223-.967-.604-1.887-1.135-2.734-.232-.37-.196-.849.089-1.181l1.529-1.784-1.52-1.52-1.784 1.529c-.332.286-.81.321-1.181.089-.848-.53-1.768-.912-2.734-1.135-.426-.099-.739-.462-.772-.898l-.18-2.341-2.148.001-.18 2.34c-.033.437-.347.8-.772.898-.967.223-1.887.604-2.734 1.135-.37.232-.849.197-1.181-.089l-1.785-1.529-1.52 1.52 1.529 1.784c.285.332.321.811.089 1.181-.53.848-.912 1.768-1.135 2.734-.099.426-.462.739-.898.772l-2.341.18.002 2.148 2.34.18c.437.033.8.347.898.772.223.967.604 1.887 1.135 2.734.232.37.196.849-.089 1.181l-1.529 1.784 1.52 1.52 1.784-1.529c.332-.287.813-.32 1.18-.089z">
                    </path>
                    <path id="XMLID_1645_" fill="#7D8590"
                        d="m16 23c-3.859 0-7-3.141-7-7s3.141-7 7-7 7 3.141 7 7-3.141 7-7 7zm0-12c-2.757 0-5 2.243-5 5s2.243 5 5 5 5-2.243 5-5-2.243-5-5-5z">
                    </path>
                </svg>
                Configuración Admin
            </button>
        </form>
    </div>
    <a class="btn-content" href="Salir.php">
        <span class="btn-title">Cerrar sesión</span>
    </a>

    <div class="container">
        <h1 class="text-center my-5">Bienvenido a Tu Plataforma Deportiva</h1>
        <div id="sport-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="./Imagenes/Futbol.jpg" class="d-block w-100" alt="Fútbol">
                    <div class="carousel-caption d-none d-md-block">
                        <h2>Fútbol</h2>
                        <p>Disfruta de los emocionantes partidos en vivo y repeticiones de fútbol.</p>
                        <a href="Futbol.php" class="btn btn-primary btn-lg">Acceder</a>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="./Imagenes/Baloncesto.png" class="d-block w-100" alt="Baloncesto">
                    <div class="carousel-caption d-none d-md-block">
                        <h2>Baloncesto</h2>
                        <p>Descubre los mejores momentos del baloncesto, en vivo y en repeticiones.</p>
                        <a href="Baloncesto.php" class="btn btn-primary btn-lg">Acceder</a>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="./Imagenes/Tenis.jpg" class="d-block w-100" alt="Tenis">
                    <div class="carousel-caption d-none d-md-block">
                        <h2>Tenis</h2>
                        <p>Observa las emocionantes competiciones de tenis en directo y en repeticiones.</p>
                        <a href="Tenis.php" class="btn btn-primary btn-lg">Acceder</a>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="./Imagenes/UFC.jpg" class="d-block w-100" alt="UFC">
                    <div class="carousel-caption d-none d-md-block">
                        <h2>UFC</h2>
                        <p>Vive la adrenalina de las peleas de UFC, tanto en vivo como en repeticiones.</p>
                        <a href="UFC.php" class="btn btn-primary btn-lg">Acceder</a>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#sport-carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="carousel-control-next" href="#sport-carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Siguiente</span>
            </a>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.3/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>

</html>