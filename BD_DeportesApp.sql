-- Crear la base de datos
CREATE DATABASE IF NOT EXISTS DeportesApp;

-- Usar la base de datos
USE DeportesApp;

-- Tabla de usuarios
CREATE TABLE IF NOT EXISTS usuarios (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL,
    apellidos VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    nickname VARCHAR(50) NOT NULL,
    hashed_password VARCHAR(255) NOT NULL,
    foto VARCHAR(255),
    rol ENUM('user', 'admin') DEFAULT 'user' NOT NULL
);

INSERT INTO usuarios (nombre, apellidos, email, nickname, hashed_password, foto, rol)
VALUES ('Florentino', 'Perez', 'florentinoperez@gmail.com', 'Floren', '$2y$15$HASH_DE_LA_CONTRASEÑA', '', 'admin');


-- Tabla de equipos de fútbol
CREATE TABLE IF NOT EXISTS equipos_futbol (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    liga VARCHAR(100) NOT NULL,
    fundacion DATE,
    estadio VARCHAR(100),
    ubicacion VARCHAR(100)
);

-- Tabla de equipos de NBA
CREATE TABLE IF NOT EXISTS equipos_nba (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    conferencia ENUM('Este', 'Oeste') NOT NULL,
    fundacion DATE,
    estadio VARCHAR(100),
    ubicacion VARCHAR(100)
);

-- Tabla de luchadores de UFC
CREATE TABLE IF NOT EXISTS luchadores_ufc (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    peso INT,
    categoria ENUM('Peso mosca', 'Peso gallo', 'Peso pluma', 'Peso ligero', 'Peso welter', 'Peso mediano', 'Peso semipesado', 'Peso pesado') NOT NULL,
    altura DECIMAL(4,2),
    pais VARCHAR(100)
);

-- Tabla de jugadores de tenis
CREATE TABLE IF NOT EXISTS jugadores_tenis (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    nacionalidad VARCHAR(100),
    ranking INT,
    fecha_nacimiento DATE,
    altura DECIMAL(4,2),
    peso INT
);

-- Tabla de eventos
CREATE TABLE IF NOT EXISTS eventos (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    deporte ENUM('Fútbol', 'Baloncesto', 'UFC', 'Tenis') NOT NULL,
    tipo ENUM('Partido', 'Competición', 'Evento especial') NOT NULL,
    fecha_hora DATETIME NOT NULL,
    lugar VARCHAR(100),
    descripcion TEXT
);

-- Futuros partidos Futbol
INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Real Sociedad vs Real Madrid', 'Fútbol', 'Partido', '2024-04-26 21:00:00', 'Reale Arena', 'El esperado enfrentamiento entre Real Sociedad y Real Madrid promete ser un emocionante duelo lleno de acción y pasión futbolística. 
Ambos equipos, conocidos por su estilo de juego vibrante y sus destacadas habilidades técnicas, se enfrentarán en un encuentro que seguramente mantendrá a los aficionados al borde de sus asientos. Con jugadores estrella en ambas filas 
y el honor en juego, este partido promete ser un espectáculo inolvidable que dejará huella en la historia del fútbol.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Atlético de MAdrid vs Athletic Club de Bilbao', 'Fútbol', 'Partido', '2024-04-27 21:00:00', 'Cívitas Metropolitano', 'Un emocionante enfrentamiento entre dos titanes del fútbol español. El Athletic Club y el Atletico de Madrid 
se disputarán el título en un partido lleno de intensidad y habilidad. Con jugadores de clase mundial en ambos equipos, esta batalla promete ser un espectáculo épico que mantendrá a los aficionados al borde de sus asientos.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Juventus vs Milan', 'Fútbol', 'Partido', '2024-04-27 18:00:00', 'Allianz Stadium', 'Un enfrentamiento emocionante entre dos de los equipos más grandes del fútbol italiano. La Juventus y el Milan se enfrentarán en el emblemático 
Allianz Stadium en un partido lleno de pasión y rivalidad. Con estrellas del fútbol mundial en ambos lados, los fanáticos esperan un encuentro lleno de acción y momentos emocionantes. ¡No te pierdas este choque entre dos gigantes del fútbol italiano!');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Bolonia vs Udinese', 'Fútbol', 'Partido', '2024-04-28 15:00:00', 'Stadio Renato Dall\'Ara', 'Un emocionante partido de fútbol entre Bolonia y Udinese que tendrá lugar en el histórico Stadio Renato Dall\'Ara. Ambos equipos buscarán la victoria 
en este encuentro, ofreciendo a los aficionados una emocionante exhibición de habilidades y estrategias en el terreno de juego. No te pierdas este emocionante enfrentamiento entre dos equipos italianos en busca de la gloria futbolística.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Manchester United vs Burnley', 'Fútbol', 'Partido', '2024-04-27 16:00:00', 'Old Trafford', 'El Manchester United se enfrenta al Burnley en un emocionante partido de fútbol en el legendario estadio de Old Trafford. Los aficionados pueden 
esperar un enfrentamiento lleno de acción y emoción mientras ambos equipos luchan por la victoria. Con estrellas del fútbol en ambos lados, este partido promete ser un espectáculo inolvidable para todos los amantes del fútbol.');

-- Partidos ya jugados Futbol
INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Manchester City vs Real Madrid', 'Fútbol', 'Partido', '2024-04-17 21:00:00', 'Etihad Stadium', 'El Real Madrid sorprendió al Manchester City con un gol tempranero de Rodrigo, igualado más tarde por De Bruyne.
 A pesar del dominio del City, el Madrid ganó en penaltis, avanzando en la Champions League.');
 
 INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Barcelona vs Real Madrid', 'Fútbol', 'Partido', '2023-10-23 16:00:00', 'Estadio Montjuïc', 'El Barcelona comenzó con ventaja, pero Jude Bellingham igualó el partido con un golazo desde fuera del área. A pesar del esfuerzo del Barcelona, 
el Real Madrid se llevó la victoria con un gol en el descuento.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Borussia Dortmund vs Atlético de Madrid', 'Fútbol', 'Partido', '2024-04-16 21:00:00', 'Signal Iduna Park', 'Un emocionante partido con muchos goles en el que ambos equipos tuvieron oportunidades para llevarse la eliminatoria. Sin embargo,
 la eficacia de los alemanes fue determinante para decidir el resultado final y la eliminatoria.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Rayo Vallecano vs Sevilla', 'Fútbol', 'Partido', '2024-05-12 20:00:00', 'Estadio de Vallecas', 'El Rayo Vallecano logra una importante victoria sobre el Sevilla gracias a un golazo de Isi Palazón, que distancia al Rayo del descenso y brinda 
esperanza a sus aficionados.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Valencia vs Betis', 'Fútbol', 'Partido', '2024-04-20 20:00:00', 'Mestalla', 'El Betis consigue una victoria crucial en Mestalla al vencer al Valencia con un impresionante doblete de Adoye Pérez. Este resultado les permite al Betis asegurar 
una posición en puestos europeos, destacando una actuación dominante frente a su rival en este emocionante encuentro de la liga española.');

-- Futuros partidos Baloncesto
INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Real Madrid vs Baskonia', 'Baloncesto', 'Partido', '2024-04-25 21:00:00', 'WiZink Center', 'El emocionante enfrentamiento entre Real Madrid y Baskonia promete ser un intenso duelo lleno de acción y estrategia en la cancha de baloncesto. 
Ambos equipos, conocidos por su calidad y determinación, se enfrentarán en un partido que seguramente mantendrá a los aficionados al borde de sus asientos. Con jugadores estrella en ambas filas y el orgullo en juego, este encuentro promete ser 
un espectáculo inolvidable que deleitará a los amantes del baloncesto.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Barcelona vs Olympiacos', 'Baloncesto', 'Partido', '2024-04-25 21:00:00', 'Palau Blaugrana', 'El emocionante enfrentamiento entre Barcelona y Olympiacos promete ser un intenso duelo lleno de acción y estrategia en la cancha de baloncesto.
 Ambos equipos, conocidos por su calidad y determinación, se enfrentarán en un partido que seguramente mantendrá a los aficionados al borde de sus asientos. Con jugadores estrella en ambas filas y el orgullo en juego, este encuentro promete ser un 
 espectáculo inolvidable que deleitará a los amantes del baloncesto.');
 
 INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Real Madrid vs Monbus Obradoiro', 'Baloncesto', 'Partido', '2024-04-28 19:00:00', 'WiZink Center', 'El emocionante enfrentamiento entre Real Madrid y Monbus Obradoiro promete ser un intenso duelo lleno de acción y estrategia en la cancha 
de baloncesto. Ambos equipos, conocidos por su calidad y determinación, se enfrentarán en un partido que seguramente mantendrá a los aficionados al borde de sus asientos. Con jugadores estrella en ambas filas y el orgullo en juego, este encuentro 
promete ser un espectáculo inolvidable que deleitará a los amantes del baloncesto.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Los Angeles Lakers vs Denver Nuggets', 'Baloncesto', 'Partido', '2024-04-28 04:00:00', 'Staples Center', 'El esperado enfrentamiento entre Los Angeles Lakers y Denver Nuggets promete ser un emocionante duelo lleno de acción y competencia 
en la cancha de baloncesto. Ambos equipos, conocidos por su talento y estilo de juego dinámico, se enfrentarán en un partido que seguramente mantendrá a los aficionados al borde de sus asientos. Con jugadores destacados en ambas filas y la 
determinación de ganar, este encuentro promete ser un espectáculo inolvidable que cautivará a los amantes del baloncesto.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Dallas Mavericks vs Los Angeles Clippers', 'Baloncesto', 'Partido', '2024-04-28 02:00:00', 'American Airlines Center', 'El emocionante enfrentamiento entre Dallas Mavericks y Los Angeles Clippers promete ser un espectáculo lleno de acción 
y rivalidad en la cancha de baloncesto. Ambos equipos, con una historia de intensos encuentros, se enfrentarán en un partido que seguramente mantendrá a los aficionados al borde de sus asientos. Con estrellas en ambos lados y el deseo de la victoria, 
este partido promete ser una batalla memorable en la temporada de baloncesto.');

-- Partidos ya jugados Baloncesto
INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Chicago Bulls vs Atlanta Hawks', 'Baloncesto', 'Partido', '2024-04-25 04:00:00', 'United Center', 'El emocionante enfrentamiento entre Chicago Bulls y Atlanta Hawks promete ser un espectáculo lleno de acción y rivalidad en la cancha de baloncesto.
 Ambos equipos, con una historia de intensos encuentros, se enfrentarán en un partido que seguramente mantendrá a los aficionados al borde de sus asientos. Con estrellas en ambos lados y el deseo de la victoria, este partido promete ser una batalla memorable 
 en la temporada de baloncesto.');
 
 INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('New Orleans Pelicans vs Sacramento Kings', 'Baloncesto', 'Partido', '2024-04-25 02:00:00', 'Smoothie King Center', 'El emocionante enfrentamiento entre New Orleans Pelicans y Sacramento Kings promete ser un espectáculo lleno de acción y emoción en la 
cancha de baloncesto. Ambos equipos, con jugadores talentosos y una fuerte determinación, se enfrentarán en un partido que seguramente mantendrá a los aficionados al borde de sus asientos. Con el deseo de la victoria y el orgullo en juego, este partido será una 
batalla memorable en la temporada de baloncesto.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Philadelphia 76ers vs New York Knicks', 'Baloncesto', 'Partido', '2024-04-26 04:00:00', 'Wells Fargo Center', 'El enfrentamiento entre Philadelphia 76ers y New York Knicks promete ser un emocionante partido de baloncesto lleno de acción y competencia. 
Con jugadores estrella en ambos equipos y un ambiente electrizante en el Wells Fargo Center, los aficionados pueden esperar una noche llena de grandes jugadas, intensidad y rivalidad en la cancha. Este partido será crucial para ambos equipos en su camino hacia 
los playoffs, y cada posesión será crucial en la búsqueda de la victoria.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Milwaukee Bucks vs Indiana Pacers', 'Baloncesto', 'Partido', '2024-04-26 02:00:00', 'Fiserv Forum', 'El emocionante enfrentamiento entre Milwaukee Bucks y Indiana Pacers promete ser un espectáculo de baloncesto lleno de acción y competencia. Con ambos 
equipos luchando por una victoria crucial en la temporada, los aficionados pueden esperar un juego lleno de emociones y grandes jugadas. El Fiserv Forum estará repleto de energía mientras los Bucks y los Pacers se enfrentan en la cancha en busca de la victoria.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Cleveland Cavaliers vs Orlando Magic', 'Baloncesto', 'Partido', '2024-04-24 04:00:00', 'Rocket Mortgage FieldHouse', 'El enfrentamiento entre Cleveland Cavaliers y Orlando Magic promete ser un emocionante duelo de baloncesto. Ambos equipos buscarán 
asegurar una victoria crucial en esta etapa de la temporada, lo que garantiza una competencia intensa en la cancha. Los aficionados pueden esperar un partido lleno de acción y grandes jugadas en el Rocket Mortgage FieldHouse.');

-- Partidos Futuros Tenis
INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Altmaier vs Landaluce', 'Tenis', 'Partido', '2024-04-26 16:00:00', 'Caja Mágica', 'El emocionante enfrentamiento entre Altmaier y Landaluce promete ser un espectáculo lleno de intensidad y habilidad tenística. Ambos jugadores han demostrado su destreza en la 
cancha y están listos para competir en un duelo que mantendrá a los espectadores al borde de sus asientos. ¡No te pierdas este emocionante partido de tenis!');
 
 INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Bautista vs Norrie', 'Tenis', 'Partido', '2024-04-26 16:00:00', 'Barcelona', 'El enfrentamiento entre Bautista y Norrie en Barcelona promete ser un emocionante duelo tenístico. Ambos jugadores han demostrado su habilidad en la cancha y están listos para 
competir en un partido que mantendrá a los aficionados al borde de sus asientos. No te pierdas este emocionante encuentro de tenis en una de las ciudades más vibrantes del mundo.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Ofner vs Tsitsipas', 'Tenis', 'Partido', '2024-04-26 16:00:00', 'Barcelona', 'El emocionante enfrentamiento entre Ofner y Tsitsipas en Barcelona promete ser un espectáculo tenístico lleno de acción y habilidad. Ambos jugadores son conocidos por su talento 
en la cancha y están listos para competir en un partido que seguramente será muy disputado. No te pierdas esta emocionante batalla tenística en una de las ciudades más importantes del mundo.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Davidovich vs Lajovic', 'Tenis', 'Partido', '2024-04-26 16:00:00', 'Barcelona', 'El esperado enfrentamiento entre Davidovich y Lajovic en Barcelona promete ser un emocionante duelo tenístico lleno de acción y pasión. Ambos jugadores son conocidos por su 
talento en la cancha y su capacidad para desplegar un juego emocionante. No te pierdas esta oportunidad de presenciar un gran partido de tenis en una de las ciudades más importantes del mundo.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Miñaur vs Nadal', 'Tenis', 'Partido', '2024-04-26 16:00:00', 'Barcelona', 'El emocionante encuentro entre Miñaur y Nadal en Barcelona promete ser un enfrentamiento épico en la cancha de tenis. Con la habilidad y destreza de Nadal frente a la energía y 
determinación de Miñaur, los fanáticos del tenis pueden esperar un espectáculo lleno de acción y tensión. No te pierdas esta oportunidad de presenciar a dos grandes del tenis mundial en acción.');

-- Partidos ya jugados Tenis
INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Nadal Wimbledon 2006', 'Tenis', 'Competicion', '2006-07-03 19:00:00', 'All England Lawn Tennis and Croquet Club, Londres', 'El legendario enfrentamiento entre Rafael Nadal y Wimbledon en el año 2006 marcó un hito en la historia del tenis. Nadal, conocido por 
su juego en arcilla, desafió las expectativas al llegar a la final en el césped de Wimbledon, enfrentándose a algunos de los mejores jugadores del mundo. Este evento histórico demostró la versatilidad y el talento excepcional de Nadal en todas las superficies de 
la cancha, y sigue siendo recordado como uno de los momentos más emocionantes del tenis moderno.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Nadal Wimbledon 2010', 'Tenis', 'Competición', '2010-06-21 19:00:00', 'All England Lawn Tennis and Croquet Club, Londres', 'El enfrentamiento entre Rafael Nadal y Wimbledon en 2010 fue otro hito en la carrera del tenista español. En este torneo,
 Nadal demostró una vez más su dominio en la superficie de césped al ganar el prestigioso campeonato de Wimbledon. Su victoria fue una exhibición de habilidad, determinación y talento, consolidando aún más su lugar en la historia del tenis.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Nadal vs Berdych - Final de Wimbledon 2010', 'Tenis', 'Partido', '2010-07-04 14:00:00', 'All England Lawn Tennis and Croquet Club, Londres', 'La final del torneo de Wimbledon en 2010 entre Rafael Nadal y Tomas Berdych fue un emocionante 
enfrentamiento que capturó la atención del mundo del tenis. Nadal, conocido por su dominio en la arcilla, demostró su versatilidad y habilidad al ganar el prestigioso título en césped. Berdych, por su parte, mostró un juego poderoso y agresivo a lo 
largo del torneo, lo que lo llevó a la final contra Nadal. El partido fue una batalla épica entre dos grandes tenistas, pero al final, Nadal emergió como el campeón, consolidando su lugar en la historia del tenis.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Novak Djokovic Wimbledon 2021', 'Tenis', 'Competición', '2021-07-11 14:00:00', 'All England Lawn Tennis and Croquet Club, Londres', 'El torneo de Wimbledon en 2021 fue una emocionante competición que vio a Novak Djokovic enfrentarse a los mejores tenistas
 del mundo en su búsqueda por otro título en el césped de Londres. Djokovic, uno de los jugadores más exitosos en la historia del tenis, mostró su habilidad y determinación en cada partido mientras luchaba por la victoria. El evento atrajo a una audiencia global y
 se destacó como uno de los momentos más destacados del calendario tenístico.');
 
INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Novak Djokovic vs Roger Federer - Final Wimbledon 2019', 'Tenis', 'Partido', '2019-07-14 14:00:00', 'All England Lawn Tennis and Croquet Club, Londres', 'La final del torneo de Wimbledon en 2019 enfrentó a dos leyendas del tenis, Novak Djokovic y Roger 
Federer, en un épico duelo por el título. El encuentro, disputado en el icónico césped de Wimbledon, fue una batalla emocionante que mantuvo a los espectadores al borde de sus asientos durante horas. Djokovic y Federer demostraron su clase y determinación en cada 
punto, creando momentos inolvidables que quedarán grabados en la historia del tenis.');

-- Partidos Fututros UFC
INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Mikel Fernández vs. Wattana', 'UFC', 'Competición', '2024-04-29 21:00:00', 'Estadio del Dragón', '¡Prepárate para la batalla del año! Mikel Fernández y Wattana se enfrentarán en el octágono en una noche llena de emoción y adrenalina. Ambos luchadores son 
conocidos por su estilo agresivo y determinación, lo que promete un enfrentamiento épico. No te pierdas este emocionante combate que dejará al público sin aliento!');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Oktagon56', 'UFC', 'Competición', '2024-07-15 20:00:00', 'Ubicación por confirmar', 'Oktagon56 presenta una noche de acción inigualable con algunos de los luchadores más talentosos del mundo. Los fanáticos pueden esperar peleas emocionantes, rivalidades intensas
 y momentos impactantes en cada combate. ¡Prepárate para una experiencia de UFC que te mantendrá al borde de tu asiento durante toda la noche!');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('PFL 3', 'UFC', 'Competición', '2024-05-20 18:00:00', 'Estadio de la Ciudad', '¡La Professional Fighters League regresa con el evento PFL 3! Los mejores luchadores del mundo se enfrentarán en una serie de combates emocionantes en busca de la gloria y la oportunidad de 
avanzar en el torneo. Desde emocionantes enfrentamientos en el suelo hasta impresionantes exhibiciones de habilidades de pie, este evento promete acción sin parar y momentos inolvidables. ¡No te lo pierdas!');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Oktagon 57', 'UFC', 'Competición', '2024-05-15 20:00:00', 'Arena Oktagon', '¡Prepárate para la acción en Oktagon 57! Los mejores luchadores de Artes Marciales Mixtas se reunirán en la emocionante Arena Oktagon para competir en una serie de combates intensos. Desde emocionantes 
enfrentamientos en el suelo hasta impresionantes exhibiciones de habilidades de pie, este evento promete mantener a los espectadores al borde de sus asientos durante toda la noche. ¡No te pierdas esta batalla épica de fuerza, habilidad y determinación en Oktagon 57!');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Nicolau vs. Perez', 'UFC', 'Competición', '2024-05-20 21:00:00', 'Estadio de la Ciudad', '¡Prepárate para presenciar uno de los combates más emocionantes del año! En este enfrentamiento de Artes Marciales Mixtas, los luchadores Nicolau y Perez se enfrentarán
 cara a cara en una batalla épica por la supremacía en el octágono. Ambos competidores han entrenado duro y están listos para darlo todo sobre el ring. Desde técnicas de lucha en el suelo hasta golpes devastadores, este combate promete mantener a los espectadores al borde de sus asientos.
 ¡No te pierdas esta oportunidad de presenciar un espectáculo de habilidad, fuerza y determinación en el Estadio de la Ciudad!');

-- Partidos ya jugados UFC
INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Toupuria vs. Volkanovsky', 'UFC', 'Competición', '2024-02-19 07:00:00', 'Centro Deportivo Local', '¡Prepárate para la batalla más esperada del año en el mundo de las Artes Marciales Mixtas! Toupuria y Volkanovsky, dos titanes del octágono, se enfrentarán en un combate épico que 
promete dejar al público sin aliento. Ambos luchadores han demostrado su destreza y determinación en numerosos combates, y ahora se preparan para enfrentarse cara a cara en el Centro Deportivo Local. Desde impactantes golpes hasta hábiles técnicas de sumisión, este combate promete ser una 
exhibición de habilidad y coraje. ¡No te pierdas la acción y la emoción de Toupuria vs. Volkanovsky!');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Brave CF 81', 'UFC', 'Competición', '2024-07-15 18:00:00', 'Estadio Arena', '¡Bienvenidos al emocionante mundo de las Artes Marciales Mixtas con Brave CF 81! Prepárate para una noche llena de acción, adrenalina y luchas épicas en el Estadio Arena. Los mejores luchadores del mundo 
se enfrentarán en el octágono, mostrando su habilidad, coraje y determinación en cada combate. Desde impactantes nocauts hasta impresionantes sumisiones, este evento promete mantener a los fanáticos al borde de sus asientos. ¡No te pierdas la oportunidad de presenciar la emoción de Brave CF 
81 en vivo!');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('PFL3 - Pesaje', 'UFC', 'Competición', '2023-08-20 00:00:00', 'Centro de Convenciones', '¡Es hora de pesar! El evento PFL3-Pesaje reúne a los luchadores más valientes y decididos de las Artes Marciales Mixtas (MMA) antes de su enfrentamiento en la jaula. Únete a nosotros en el 
Centro de Convenciones, donde cada atleta se someterá al riguroso proceso de pesaje para garantizar que estén listos y en forma para la competencia. Experimenta la tensión y la emoción mientras los luchadores hacen frente a la balanza y se preparan para hacer historia en el octágono. 
¡No te pierdas el evento PFL3-Pesaje, donde se forjan los campeones!');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('Ryan vs. Victor', 'UFC', 'Competición', '2023-08-25 05:00:00', 'Estadio de Las Vegas', '¡Prepárate para la batalla del año! El evento "Ryan vs. Victor" promete ser una noche llena de acción y emoción en el octágono. Dos guerreros del octágono, Ryan y Victor, se enfrentarán cara a
 cara en una batalla épica por la supremacía en la UFC. Ambos luchadores están en la cima de su juego, con habilidades excepcionales y un deseo inquebrantable de la victoria. Únete a nosotros en el Estadio de Las Vegas y sé testigo de momentos de pura adrenalina mientras estos titanes de
 las Artes Marciales Mixtas se enfrentan en una lucha que será recordada por generaciones.');

INSERT INTO eventos (nombre, deporte, tipo, fecha_hora, lugar, descripcion) 
VALUES ('PFL10 - Playoff Finals', 'UFC', 'Competición', '2024-01-15 02:00:00', 'Estadio PFL, Miami', '¡Es la gran final de los playoffs de la PFL! Los mejores luchadores de Artes Marciales Mixtas se enfrentarán en una emocionante noche llena de acción en el Estadio PFL de Miami. Después de
 una temporada de combates intensos, los contendientes más destacados han llegado a la final, y solo uno se coronará como campeón. Prepárate para presenciar combates épicos, momentos de suspenso y un despliegue de habilidades atléticas mientras los luchadores luchan por la gloria y el título 
 de campeón de la Professional Fighters League.');





 
 



