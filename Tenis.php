<?php
session_start();

// Verificar si el usuario está autenticado
if (!isset($_SESSION['usuario'])) {
    // Si el usuario no está autenticado, redirigirlo a la página de inicio de sesión
    header("Location: PaginaInicio.php");
    exit;
}

require_once 'FuncionSql.php';
$conn = baseconexion();

// Verificar si se ha establecido una sesión de usuario
if (isset($_SESSION['usuario'])) {
    // Obtener el usuario de la sesión
    $usuario = $_SESSION['usuario'];

 
} else {
    // Si no hay sesión de usuario, redirigir a la página de inicio de sesión
    header("Location: PaginaInicio.php");
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página de Acceso</title>
    <link rel="stylesheet" href="Tenis.css">
</head>

<body>
    <div class="container">
        <h1 class="title">Página de Acceso</h1>
        <h2 class="slide-title">Tenis</h2>
        <div>
            <h1>Proximos partidos</h1>
        </div>
        <div class="matches">
            <a class="match" href="PartidosTenis/Altmaier-Landaluce.php">
                <div class="match">
                    <img src="./Imagenes/Altmaier - Landaluce.jfif" alt="Partido 1">
                    <h4>Altmaier - Landaluce</h4>
                </div>
            </a>
            <a class="match" href="PartidosTenis/Bautista-Norrie.php">
                <div class="match">
                    <img src="./Imagenes/Bautista - Norrie.jfif" alt="Partido 2">
                    <h4>Bautista - Norrie</h4>
                </div>
            </a>
            <a class="match" href="PartidosTenis/Ofner-Tsitsipas.php">
                <div class="match">
                    <img src="./Imagenes/2ª Ronda Ofner - Tsitsipas.jfif" alt="Partido 3">
                    <h4>2ª Ronda: Ofner - Tsitsipas</h4>
                </div>
            </a>
            <a class="match" href="PartidosTenis/Davidovich-Lajovic.php">
                <div class="match">
                    <img src="./Imagenes/Octavos de final Davidovich - Lajovic.jfif" alt="Partido 4">
                    <h4>Octavos de final: Davidovich - Lajovic</h4>
                </div>
            </a>
            <a class="match" href="PartidosTenis/Miñaur-Nadal.php">
                <div class="match">
                    <img src="./Imagenes/2ª Ronda De Miñaur - Nadal.jfif" alt="Partido 5">
                    <h4>2ª Ronda: De Miñaur - Nadal</h4>
                </div>
            </a>

        </div>

        <div>
            <h1>Mejores Partidos</h1>
        </div>
        <div></div>
        <div class="matches">
            <a class="match" href="PartidosTenis/Nadal-Wimbledon-2006.php">
                <div class="replay">

                    <img src="./Imagenes/Película oficial de Wimbledon 2006.jfif" alt="Repetición 1">
                    <h4>Película oficial de Wimbledon 2006</h4>
                </div>
            </a>
            <a class="match" href="PartidosTenis/Nadal-Wimbledon-2010.php">
                <div class="replay">
                    <img src="./Imagenes/Película oficial de Wimbledon 2010.jfif" alt="Repetición 2">
                    <h4>Película oficial de Wimbledon 2010</h4>
                </div>
            </a>
            <a class="match" href="PartidosTenis/Nadal-Berdych.php">
                <div class="replay">
                    <img src="./Imagenes/Wimbledon R. Nadal - T. Berdych. Final Masculina.jfif" alt="Repetición 3">
                    <h4>Wimbledon (2010): R. Nadal - T. Berdych</h4>
                </div>
            </a>
            <a class="match" href="PartidosTenis/Novak-Wimbledon-2021.php">
                <div class="replay">
                    <img src="./Imagenes/Película Oficial de Wimbledon 2021.jfif" alt="Repetición 4">
                    <h4>Película Oficial de Wimbledon 2021</h4>
                </div>
            </a>
            <a class="match" href="PartidosTenis/Novak-Federer.php">
                <div class="replay">
                    <img src="./Imagenes/N. Djokovic - R. Federer.jfif" alt="Repetición 5">
                    <h4>N. Djokovic - R. Federer</h4>
                </div>
            </a>

        </div>
    </div>
    </div>
</body>

</html>